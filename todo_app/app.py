from audioop import add
from flask import Flask, render_template, request, redirect, url_for

from todo_app.flask_config import Config
from todo_app.data.session_items import delete_item, get_items, add_item

app = Flask(__name__)
app.config.from_object(Config())


@app.route('/')
def index():
    items = get_items()
    return render_template('index.html', items=items)

@app.route('/add', methods=['POST'])
def add_item_to_list():
    newEntryTitle = request.form['title']
    add_item(newEntryTitle)
    return redirect(url_for('index'))

@app.route('/delete')
def index_delete():
    items = get_items()
    return render_template('delete.html', items=items)

@app.route('/delete', methods=['POST'])
def delete_item_from_list():
    idToDelete = request.form['id']
    anythingDeleted = delete_item(idToDelete)
    return redirect(url_for('index_delete'))

if __name__ == '__main__':
    app.run()
