from flask import session

_DEFAULT_ITEMS = [
    { 'id': 1, 'status': 'Not Started', 'title': 'List saved todo items' },
    { 'id': 2, 'status': 'Not Started', 'title': 'Allow new items to be added' }
]


def get_items():
    """
    Fetches all saved items from the session.

    Returns:
        list: The list of saved items.
    """
    return session.get('items', _DEFAULT_ITEMS.copy())


def get_item(id):
    """
    Fetches the saved item with the specified ID.

    Args:
        id: The ID of the item.

    Returns:
        item: The saved item, or None if no items match the specified ID.
    """
    items = get_items()
    return next((item for item in items if item['id'] == int(id)), None)


def add_item(title):
    """
    Adds a new item with the specified title to the session.

    Args:
        title: The title of the item.

    Returns:
        item: The saved item.
    """
    items = get_items()

    # Determine the ID for the item based on that of the previously added item
    id = items[-1]['id'] + 1 if items else 1

    item = { 'id': id, 'title': title, 'status': 'Not Started' }

    # Add the item to the list
    items.append(item)
    session['items'] = items

    return item


def save_item(item):
    """
    Updates an existing item in the session. If no existing item matches the ID of the specified item, nothing is saved.

    Args:
        item: The item to save.
    """
    existing_items = get_items()
    updated_items = [item if item['id'] == existing_item['id'] else existing_item for existing_item in existing_items]

    session['items'] = updated_items

    return item

def delete_item(id):
    """
    Deletes one (or all) item with the specified id to the session.

    Args:
        id: The id of the item.

    Returns:
        result: execution code.
    """
    items = get_items()

    if id=="all":
        print('deleting everything')
        items = _DEFAULT_ITEMS
        session['items'] = items
        return 0
    else:
        try:
            idAsInt = int(id)
            origLen = len(items)
            items[:] = [entry for entry in items if entry.get('id') != idAsInt]
            newLen = len(items)
            session['items'] = items
            if origLen==newLen:
                print('deleted nothing, most likely because such ID does not exist')
                return 1
            return 0
        except ValueError:
            print('Please enter an integer')
            return -1